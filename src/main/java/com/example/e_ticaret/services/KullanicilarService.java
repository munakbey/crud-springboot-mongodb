package com.example.e_ticaret.services;

import com.example.e_ticaret.models.Kullanicilar;

import java.util.List;

public interface KullanicilarService {
 /*
    void silKullanici(Kullanicilar kullanici);
    void guncelleKullanici(Kullanicilar kullanici);*/
 void ekleKullanici(Kullanicilar kullanici);
    List<Kullanicilar> kullaniciListele();
    Kullanicilar getKullanicilarByAd(String ad);
    void guncelleKullanici(Kullanicilar kullanici);
    void silKullanici(String id);
}
