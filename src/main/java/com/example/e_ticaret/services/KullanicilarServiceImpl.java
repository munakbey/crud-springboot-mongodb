package com.example.e_ticaret.services;

import com.example.e_ticaret.models.Kullanicilar;
import com.example.e_ticaret.repositories.KullanicilarRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class KullanicilarServiceImpl implements  KullanicilarService {

    @Autowired
    KullanicilarRepo kullanicilarRepo;

    @Override
    public void ekleKullanici(Kullanicilar kullanici) {
        kullanicilarRepo.save(kullanici);
    }

    @Override
    public List<Kullanicilar> kullaniciListele() {
        return kullanicilarRepo.findAll();
    }

    @Override
    public Kullanicilar getKullanicilarByAd(String ad) {
        return kullanicilarRepo.findKullanicilarByAd(ad);
    }

    @Override
    public void guncelleKullanici(Kullanicilar kullanici) {
        kullanicilarRepo.save(kullanici);
    }

    @Override
    public void silKullanici(String id) {
        kullanicilarRepo.deleteById(id);
    }
}
