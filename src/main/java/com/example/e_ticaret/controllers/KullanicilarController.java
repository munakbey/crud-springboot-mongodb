package com.example.e_ticaret.controllers;

import com.example.e_ticaret.models.Kullanicilar;
import com.example.e_ticaret.services.KullanicilarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController//returndeki yazılanı yapar  dumduz
//@Controller
@RequestMapping("/api")
@CrossOrigin(origins="http://localhost:4202",allowedHeaders = "*")
public class KullanicilarController {

    @Autowired
    private KullanicilarService kullanicilar_service;

    @GetMapping("kullanicilar/get/all")
    public List<Kullanicilar> Listele(){
 return kullanicilar_service.kullaniciListele();

    }

    @PostMapping("/kullanicilar/ekle")
    public void ekleKullanici(@RequestBody Kullanicilar kullanici){

        kullanicilar_service.ekleKullanici(kullanici);
    }

    @PutMapping("/kullanicilar/guncelle")
    public  void guncelleKullanici(@RequestBody Kullanicilar kullanici){
        kullanicilar_service.guncelleKullanici(kullanici);
    }

    @DeleteMapping("/kullanicilar/sil/{id}")
    public void silKullanicilar(@PathVariable("id") String id){
        kullanicilar_service.silKullanici(id);
    }
  /*  @RequestMapping("/index")
    public String giris(){
        return "index";
    }*/
  /*  @RequestMapping("/goster")
    public ModelAndView home(){
        ModelAndView mv=new ModelAndView("goster");
        return mv;
    }*/

}
