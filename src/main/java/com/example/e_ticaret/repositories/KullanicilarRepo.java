package com.example.e_ticaret.repositories;

import com.example.e_ticaret.models.Kullanicilar;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface KullanicilarRepo extends MongoRepository<Kullanicilar,String> {

    Kullanicilar findKullanicilarByAd(String ad);
}
